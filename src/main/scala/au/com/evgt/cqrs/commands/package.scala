package au.com.evgt.cqrs

import au.com.evgt.cqrs.Main.ProductionLineType

package object commands {

  trait Command

  sealed trait FactoryCommand extends Command

  object FactoryCommand {

    case object Shutdown extends FactoryCommand

    case object Open extends FactoryCommand

    case class AddProductionLine(productionLineType: ProductionLineType) extends FactoryCommand

    case class ReduceStaff(amount: Int) extends FactoryCommand

    case class ShutdownLine(number: Int) extends FactoryCommand

    case class StartLine(number: Int) extends FactoryCommand

    case class RepairLine(number: Int) extends FactoryCommand

  }

}
