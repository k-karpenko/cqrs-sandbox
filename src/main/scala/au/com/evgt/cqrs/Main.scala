package au.com.evgt.cqrs

import java.time.Instant

import au.com.evgt.cqrs.Main.Aggregate.CommandExecutionFailed
import au.com.evgt.cqrs.commands._
import au.com.evgt.cqrs.events._

import scala.collection.mutable

object Main extends App {

  trait CommandResult[+Evt <: Event]
  object CommandResult {
    case class Success[E <: Event](events: E*) extends CommandResult[E]
    case class Failure(reason: String) extends CommandResult[Nothing]
  }

  trait CommandValidationResult
  object CommandValidationResult {
    case object Pass extends CommandValidationResult
    case class Invalid(reason: String) extends CommandValidationResult
  }

  sealed trait FactoryType {
    val personnelCapacity: Int
    val productionLinesLimit: Int
  }

  object FactoryType {
    case object Small extends FactoryType {
      override val personnelCapacity = 10
      override val productionLinesLimit = 3
    }

    case object Medium extends FactoryType {
      override val personnelCapacity = 50
      override val productionLinesLimit = 5
    }

    case object Large extends FactoryType {
      override val personnelCapacity = 150
      override val productionLinesLimit = 8
    }
  }

  sealed trait ProductionLineType {
    val personnelRequired: Int
  }

  object ProductionLineType {
    case object Small extends ProductionLineType {
      override val personnelRequired = 2
    }

    case object Medium extends ProductionLineType {
      override val personnelRequired = 5
    }

    case object Large extends ProductionLineType {
      override val personnelRequired = 20
    }
  }

  case class EmployeeContract(isActive: Boolean, salaryPackage: Int, hiredAt: Instant, closedAt: Instant)

  case class Employee(contract: EmployeeContract)

  object ProductionLine {
    def default(): ProductionLine = ProductionLine(isStarted = false, ProductionLineType.Small, Seq.empty)
  }

  case class ProductionLine(isStarted: Boolean, lineType: ProductionLineType, assignees: Seq[Employee])

  sealed trait FactoryStatus
  object FactoryStatus {
    case class Open(at: Instant) extends FactoryStatus
    case class Closed(at: Instant) extends FactoryStatus
  }

  object Factory {
    case class Id(value: String)
  }

  case class Factory(status: FactoryStatus,
                     factoryType: FactoryType,
                     productionLines: Seq[ProductionLine])

  object Aggregate {

    case class CommandExecutionFailed(reason: String) extends Throwable

  }

  val events: mutable.Map[String, Seq[Event]] = mutable.HashMap()
  val persistentViews: mutable.Map[String, Seq[PersistentView[_]]] = mutable.HashMap()
  val writeLock = new Object()

  trait PersistentView[State] {
    private var _state: Option[State] = None

    def persistentId: String

    protected def state: Option[State] = _state

    protected def updatedState(state: Option[State]): PartialFunction[Event, Option[State]]

    def onEvent(e: Event): Unit = {
      _state = updatedState(_state)(e)
    }

    def initialize(): Unit = {
      persistentViews.update(
        persistentId,
        persistentViews.getOrElseUpdate(persistentId, List.empty[PersistentView[State]]) :+ this
      )
    }

  }

  trait PersistentStore[Evt <: Event] {
    val persistentId: String

    def addEvent[T](evt: Evt)(block: => T): T = {
      writeLock.synchronized {
        events.update(persistentId, events.getOrElseUpdate(persistentId, List.empty[Evt]) :+ evt)
        persistentViews.get(persistentId) foreach (_.foreach(_.onEvent(evt)))
        block
      }
    }
  }

  trait Aggregate[State, Cmd <: Command, Evt <: Event] extends PersistentStore[Evt] {
    private var _state: Option[State] = None

    protected def updatedState(state: Option[State]): PartialFunction[Evt, Option[State]]

    protected def commandValidator(state: Option[State]): PartialFunction[Cmd, CommandValidationResult]

    protected def commandProcessor(state: Option[State]): PartialFunction[Cmd, CommandResult[Evt]]

    def executeCommand(command: Cmd): Either[Throwable, Unit] = {
      commandProcessor(_state)(command) match {
        case CommandResult.Failure(reason) ⇒
          Left(CommandExecutionFailed(reason))
        case CommandResult.Success(list @ _*) ⇒
          _state = list.foldLeft(_state)((current, event) ⇒ addEvent(event)(updatedState(_state)(event)))
          Right({})
      }
    }
  }

  object FactoryAggregate {
    private val context: mutable.Map[Factory.Id, FactoryAggregate] = mutable.HashMap()

    def resolve(id: Factory.Id): FactoryAggregate = {
      this.synchronized {
        context.getOrElseUpdate(id, new FactoryAggregate(id))
      }
    }

  }

  class FactoryAggregate(id: Factory.Id) extends Aggregate[Factory, FactoryCommand, FactoryEvent] {

    override val persistentId: String = s"factory-${id.value}"

    override protected def updatedState(state: Option[Factory]): PartialFunction[FactoryEvent, Option[Factory]] = {
      case FactoryEvent.Opened(at) ⇒
        Some(Factory(
          FactoryStatus.Open(at),
          factoryType = FactoryType.Small,
          productionLines = Seq.empty
        ))
      case FactoryEvent.Closed(at) ⇒
        state.map(_.copy(status = FactoryStatus.Closed(at)))

      case FactoryEvent.ProductionLineAdded(productionLineType)  ⇒
        state.map(v ⇒
          v.copy(
            productionLines = v.productionLines :+ ProductionLine(isStarted = false, productionLineType, Seq.empty)
          )
        )
    }

    override def commandValidator(state: Option[Factory]): PartialFunction[FactoryCommand, CommandValidationResult] = {
      case FactoryCommand.AddProductionLine(_) ⇒
        state.fold[CommandValidationResult](CommandValidationResult.Invalid("not open")) {
          case x if x.productionLines.size < x.factoryType.productionLinesLimit ⇒
            CommandValidationResult.Pass
        }

      case FactoryCommand.Open ⇒
        state.map(_.status)
          .fold[CommandValidationResult](CommandValidationResult.Pass) {
            case _: FactoryStatus.Open ⇒ CommandValidationResult.Invalid("already open")
            case _: FactoryStatus.Closed ⇒ CommandValidationResult.Pass
          }

      case FactoryCommand.Shutdown ⇒
        state.map(_.status)
          .fold[CommandValidationResult](CommandValidationResult.Pass) {
          case _: FactoryStatus.Open ⇒ CommandValidationResult.Pass
          case _: FactoryStatus.Closed ⇒ CommandValidationResult.Invalid("closed already")
        }
    }

    override def commandProcessor(state: Option[Factory]): PartialFunction[FactoryCommand, CommandResult[FactoryEvent]] = {
      case FactoryCommand.Open ⇒
        CommandResult.Success(FactoryEvent.Opened(Instant.now))

      case FactoryCommand.Shutdown ⇒
        CommandResult.Success(FactoryEvent.Closed(Instant.now))

      case FactoryCommand.AddProductionLine(productionLineType) ⇒
        CommandResult.Success(FactoryEvent.ProductionLineAdded(productionLineType))
    }
  }

  class FactoryView(id: Factory.Id) extends PersistentView[Factory] {
    override val persistentId: String = s"factory-${id.value}"

    override protected def updatedState(state: Option[Factory]): PartialFunction[Event, Option[Factory]] = {
      case FactoryEvent.Opened(at) if state.isEmpty ⇒
        Some(Factory(FactoryStatus.Open(at), FactoryType.Small, Seq.empty))
      case FactoryEvent.Opened(at) if state.nonEmpty ⇒
        state.map(_.copy(status = FactoryStatus.Open(at)))
      case FactoryEvent.Closed(at) ⇒
        state.map(_.copy(status = FactoryStatus.Closed(at)))
      case FactoryEvent.ProductionLineAdded(productionLineType)  ⇒
        state.map(v ⇒
          v.copy(
            productionLines = v.productionLines :+ ProductionLine(isStarted = false, productionLineType, Seq.empty)
          )
        )
    }

    def listProductionLines(): Seq[ProductionLine] = {
      state.map(_.productionLines).getOrElse(Seq.empty)
    }

    initialize()
  }

  trait Saga[Evt <: Event] {

    def begin(): Either[Throwable, Unit]

    def rollback(): Unit

  }

  class FactoryOpenSaga(factoryId: Factory.Id) extends Saga[FactoryEvent] {

    override def begin(): Either[Throwable, Unit] = {
      val factory = FactoryAggregate.resolve(factoryId)
      val factoryView = new FactoryView(factoryId)

      factory.executeCommand(FactoryCommand.Open) map { _ ⇒
        factory.executeCommand(FactoryCommand.AddProductionLine(ProductionLineType.Medium)) map { _ ⇒
          factoryView.listProductionLines().map(line ⇒
            println(s"Opening line $line")
          )
        }
      }
    }

    override def rollback(): Unit = {

    }

  }

  new FactoryOpenSaga(Factory.Id("test")).begin()

}
