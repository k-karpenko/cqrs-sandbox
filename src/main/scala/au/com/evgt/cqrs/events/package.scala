package au.com.evgt.cqrs

import java.time.Instant

import au.com.evgt.cqrs.Main.ProductionLineType

package object events {

  trait Event

  sealed trait FactoryEvent extends Event

  object FactoryEvent {
    case class ProductionLineAdded(productionLineType: ProductionLineType) extends FactoryEvent
    case class Opened(at: Instant) extends FactoryEvent
    case class Closed(at: Instant) extends FactoryEvent
  }

}
